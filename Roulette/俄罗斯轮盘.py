# coding=utf-8

import tkinter as tk
from tkinter import messagebox
import random
import time
import os
import sys
import playsound

print('Game Start.')

window = tk.Tk()
window.iconbitmap("./icon.ico")
window.title('俄罗斯轮盘')
window.geometry('600x400')
window.resizable(False,False)
var = tk.StringVar()
def MIT():

    mit = '''
    本程序遵循MIT协议.
    https://opensource.org/license/MIT
    https://gitlab.com/fafaya/any-python-file/-/tree/main/Roulette

    Copyright 2024 zhipeng

    Permission is hereby granted, free of charge, 
    to any person obtaining a copy of this software and associated documentation files (the “Software”), 
    to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
    and to permit persons to whom the Software is furnished to do so, 
    subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, 
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
    DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

    '''
    messagebox.showinfo(title='开源协议', message=mit)  
def about():
    messagebox.showinfo(title='关于', message='其实就是随便写的（\n作者:zhipeng\n游戏版本:Beta-1.2')   
def restart():
    python = sys.executable
    print('restarting...')
    os.execl(python, python, * sys.argv)
def pass_button():
    messagebox.showinfo(title='你点了一个没用的按钮', message='这就是个占位按钮')   
def exit():
    sys.exit()
menubar = tk.Menu(window)
filemenu = tk.Menu(menubar, tearoff=0)
menubar.add_cascade(label='其他', menu=filemenu)
filemenu.add_command(label='关于', command=about)
filemenu.add_command(label='重启游戏', command=restart)
filemenu.add_command(label='开源协议', command=MIT)
window.config(menu=menubar)

title = tk.Label(window, text='--俄罗斯轮盘--', font=("华文行楷",48), width=10, height=2)
title.pack()
text = tk.Label(window,text='游戏规则：轮番向自己开枪，六分之一几率中枪', font=("微软雅黑",10,"bold"), width=100, height=4)
text.pack()
shot = random.choice([1, 2, 3, 4, 5, 6])
now_place = random.choice([1, 2, 3, 4, 5, 6])
print(f'shot={shot}')
print(f'now_place={now_place}')
def play():
    global now_place
    if shot == now_place:
        playsound.playsound('./shot.mp3') 
        messagebox.showwarning(title='您失败了。。。', message='中枪了！！！')  
        time.sleep(1)
        # os.system('shutdown -s -t 1')
        # 这一条是导致系统关机的，费电脑
        restart()
    else:
        playsound.playsound('./no-shot.mp3')
        messagebox.showinfo(title='逃过一劫', message='侥幸逃过一劫！')   
        now_place = now_place + 1
        if now_place > 6:
            now_place = 1
    print(f'new-now_place={now_place}')
def bot_pk():
    global shot,now_place
    window.destroy()
    bot = tk.Tk()
    bot.title('俄罗斯轮盘-人机对战模式')
    bot.geometry('200x300')
    def bot_pk_play():
        messagebox.showinfo(title='nope', message='其实bot对战还没弄好呢)')   
    tk.Button(bot, text='Play', font=("微软雅黑",10,"bold"), width=13, height=2, command=bot_pk_play).pack()
b = tk.Button(window, text='我准备好了!\n(标准模式)', font=("微软雅黑",10,"italic","bold"), width=13, height=2, command=play)
b.pack()
text1 = tk.Label(window,text="以下为测试内容（不稳定，仅供测试）：",font=("微软雅黑",10,"bold"),width=50,height=2)
text1.pack()
b2 = tk.Button(window, text='人机对战模式', font=("微软雅黑",10,"bold"), width=10, height=1, command=bot_pk)
b2.pack()
tk.Label(window, text="ver.Beta-1.2\n作者:zhipeng", bg="green", font=("微软雅黑",10,"bold"), width=15, height=2).place(x=450,y=325)
tk.Button(window, text="Exit|退出", font=("微软雅黑",10,"bold"), width=15, height=2, command=exit).place(x=20,y=325)

window.mainloop()